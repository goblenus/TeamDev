# Цель
Дать всестороннее понимание по работе в команде и разработке ПО.

# 1. Команда
Почитать:
* https://habrahabr.ru/company/edison/blog/272483/
* http://mkechinov.ru/development_team.html
* https://www.gd.ru/articles/3953-formirovanie-komandy-proekta

Команда проекта – совокупность отдельных лиц (участников проекта), привлеченных к выполнению работ проекта и ответственных перед руководителем проекта за их выполнение.

## 1.1 Участники команды

## 1.2 Организация команды
## 1.3 Роль команды в разработке ПО
# 2. Методологии разработки ПО
Почитать:
* https://habrahabr.ru/company/edison/blog/269789/
* https://habrahabr.ru/company/edison/blog/272085/
* http://agilerussia.ru/methodologies/%D0%BE%D0%B1%D0%B7%D0%BE%D1%80-%D0%BC%D0%B5%D1%82%D0%BE%D0%B4%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8-scrum/
* https://geekbrains.ru/posts/methodologies

# 3. Применение систем контроля версий
Почитать:
* https://githowto.com/ru/checking_status
* https://habrahabr.ru/post/322424/
* https://git-scm.com/book/ru/v2/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-Git
* https://habrahabr.ru/company/playrix/blog/345732/

## 3.1 Установка Git
* Linux
 
    ```
    yum install git
    ```
    
    ```
    apt install git
    ```
    
* Mac
    
    ```
    http://git-scm.com/download/mac
    ```
    
* Windows

    ```
    http://git-scm.com/download/win
    ```
    
## 3.2 Подготовка к работе

Для начала работы с git необходимо задать имя и почту
```
git config --global user.name "Your Name"
git config --global user.email "your_email@whatever.com"
```
Вы можете задать всё что угодно.

### 3.2.1 Графические надстройки над git
**!!!!**

## 3.3 Создание репозитория
Система контроля версий git в общем случае может работать и без привязки к удаляенному репозиторию,
что позволяет Вам производить разраотку не будучи подключённым к Интернет.
Внешний репозиторий позволяет Вам лишь удалённо хранить Ваш репозиторий и обмениваться кодом с 
коммандой, что если Вы разрабатываете один те так важно.

Репозиторий создаётся командой
```
git init
```
Посе выполнения данной команды появится директория .git в которой будут храниться все настройки репозитория

## 3.4 Добавление файла для отслеживания
Чтобы начать отслеживание файла, сначала его необходимо добавить в репозиторий
```
git add hello.html
```
После выполнения этой команды, файл будет добавлен в файл настроек в папке .git

## 3.5 
    
# 4. Использование GitLab при разработке ПО 
Почитать:
* https://habrahabr.ru/company/softmart/blog/309380/
* https://coursehunters.net/course/gitlab